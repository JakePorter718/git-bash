# Git Tutorial
    * First you want to install git using git bash.
    * Next you can open any file using git and turn it into a git repository by right clicking on the file and opening in git bash.
# Git Commands
> Git local Commands.
## Command : git init
    * Run the git init. to make a file into a repo it can be an empty or already a sln or project.
## Command : git status
    * This will tell you the status of your repository.
## Command : git add
    * This will let you add a file that is not in the repo as of right now.
    * You will need to use a add every time you change anything.
## Command : git commit
    * This will bring up a different type of window
    ### Edit the comment
        * Hit the I key too go into insert mode
        * Type in your comment
        * Then Hit the ESC key to exit the insert mode
    ### Finish the commit
        * Type :wq hit enter
## Command : git commit -m 'comment'
    * This will skip the vim screen and auto commit with the comment    
    * USE THIS>
## Command : git log
    * This will get the commit history
## Command : git add .
    * This will add all files not part of the repository and any that need to be readded because of changes.
## Command : git add #.htlm
    * This can add all files since it is a wild card.
## Command : touch .gitignore
    * Will create a .gitignore file for use.
    * touch will great any file.
## Command : git branch MyBranch
    * Will make a new branch called MyBranch
## Command : git checkout MyBranch
    * Will get the other branch instead of the master.
## Command : git merge MyBranch
    * This will merge your branch back to the master.
    * Make sure that master is the branch that you have checked out when doing a merge.
## Command : git commit -a -m 'change6'
    * This will commit any changes that have already been added to the project if anything is a new file you need to add it first.
## Command : git mergetool
    * This will open a merge tool in vim that will help you merge
> Find a merge tool tortoisemerge or winmerge.
## Command : git stash
    * This will stash or shelf your changes that you have not committed.
## Command : git stash apply
    * This will get the stash and unstash it applying it back to your repo.
> Git remote commands
## Command : git remote
    * This will let you see the remote repo's that you have.
## Command : git clone http********************
    * Will Clone the repo.
> A few commands for the shell (cd) go to a directory (cd..) go back a directory (ls) all available directories.
## Command : git remote -v
    * Will give you the https of the repos that are present for the application
## Command : git fetch (name of repo)
    * Will get all the changes from the remote repo and added it to the local but with no merge.
## Command : git pull (name of repo)
    * Will get all the changes and merge also.
## Command : git push (name of repo) (name of branch)
    * example git push origin master
## Command : git remote add (name of repo) (url)
    * This will add a remote repository
